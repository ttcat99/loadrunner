﻿Module Module1

    Sub Main()

        Console.WriteLine("----Welcome to LoadRunner!----")

        Dim ArgSira As String() = System.Environment.GetCommandLineArgs()
        If ArgSira.Count < 2 Then
            Console.WriteLine("Args less than 2.")
        Else
            Dim Fname As String = ArgSira(1) & ".runbat"

            Console.WriteLine("runbat Selected: " & Fname)

            If Not Dir(Fname) = "" Then
                Dim Lee As New System.IO.StreamReader(Fname)

                Dim Exe As String = lee.ReadLine()
                Console.WriteLine("Exec Selected: " & exe)

                Dim Arg As String = lee.ReadLine()
                Console.WriteLine("Args Selected: " & Arg)

                'ProcessStartInfoオブジェクトを作成する
                Dim psi As New System.Diagnostics.ProcessStartInfo()
                '起動するファイルのパスを指定する
                psi.FileName = exe
                'コマンドライン引数を指定する
                psi.Arguments = Arg

                'アプリケーションを起動する
                System.Diagnostics.Process.Start(psi)

                Console.WriteLine("Process Srarted")

                lee.Close()

            Else
                Console.WriteLine("runbat not exists...")
            End If
        End If
        Console.WriteLine("Quit LoadRunner... [END]")

    End Sub

End Module
